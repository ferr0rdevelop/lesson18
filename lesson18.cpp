#include <iostream>
#include <conio.h>
//#include <stdio.h>
using namespace std;



class Stack
{
private:
    int value = 0;
    int size = 1;
    int* array;
    bool isProgramActive = true;
public:
    Stack(int value)
    {
        array = new int[size];
        for (int i = 0; i < size; i++)
        {
            array[i] = value;
        }
        std::cout << "Array create successfully!" << "\nArray length: " << size << "\n\n";
    }
    // Main loop
    void start()
    {
        while (isProgramActive)
        {
            drawStack();
            cout << "Chose action number (or Press 0 to exit...): \n" << "1. Add new item to the stack\t2. Get and delete item from the stack\n";
            unsigned char inputChar = _getch();
            if (inputChar == '1')
            {
                cout << "Enter a value to add to the stack: ";
                cin >> value;
                push(value);
            }
            if (inputChar == '2')
            {
                cout << pop();
            }
            if (inputChar == '0')
            {
                isProgramActive = false;
            }
        }
    }
    
    // This method adds a new item to the "stack".
    void push(int value)
    {
        size++;
        cout << "New size : " << size << "\n";

        // Create new temporary "stack" and fill.
        int* tmp_array = new int[size];
        for (int i = 0; i < size; i++)
        {
            if (i == size - 1)
            {
                tmp_array[i] = value;
                break;
            }
            tmp_array[i] = array[i];
        }

        // Delete old "stack". Creates and fill a new "stack" using a temporary.
        delete[] array;
        array = nullptr;
        array = new int[size];
        for (int i = 0; i < size; i++)
        {
            array[i] = tmp_array[i];
        }

        // Delete temporary "stack".
        delete[] tmp_array;
        tmp_array = nullptr;
    }
    
    // This method removes the last item from the stack and returns it.
    int pop()
    {
        if (size == 0)
        {
            return 0;
        }
        size--;
        int tmp_value = array[size];

        // Create new temporary "stack" and fill.
        int* tmp_array = new int[size];
        cout << "\nRemove item at the top of the stack: " << array[size] << "\n\n";
        _getch();
        for (int i = 0; i < size; i++)
        {
            tmp_array[i] = array[i];
        }

        // Delete old "stack". Creates and fill a new "stack" using a temporary.
        delete[] array;
        array = nullptr;
        array = new int[size];
        for (int i = 0; i < size; i++)
        {
            array[i] = tmp_array[i];
        }

        // Delete temporary "stack".
        delete[] tmp_array;
        tmp_array = nullptr;

        return tmp_value;
    }

    // This method draws the "stack" in the console.
    void drawStack()
    {
        system("CLS");
        cout << "Stack size: " << size << "\n\n";
        for (int i = size - 1; i >= 0; i--)
        {
            cout << "| " << array[i] << "\t|  " <<  "array[" << i << "]" "\n";
            cout << "|_" << "______|\n";
        }
        cout << '\n';
    }
};
int main()
{
    Stack stack(11);
    stack.start();    
    return 0;
}